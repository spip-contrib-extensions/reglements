<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// R
	'reglements_description' => 'Permet la saisie des règlements effectués en rapport avec une facture. <br/>Ajoute une table spip_reglements comportant les champs date_reglement, id_facture, montant.',
	'reglements_nom' => 'Règlements',
	'reglements_slogan' => 'On va tout régler!',
);
