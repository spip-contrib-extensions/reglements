<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_reglement' => 'Ajouter ce règlement',

	// I
	'icone_creer_reglement' => 'Saisir un nouveau règlement',
	'icone_modifier_reglement' => 'Modifier ce règlement',
	'info_1_reglement' => 'Un règlement',
	'info_aucun_reglement' => 'Aucun règlement',
	'info_nb_reglements' => '@nb@ règlements',
	'info_reglements_auteur' => 'Les règlements de cet auteur',

	// L
	'label_commentaires' => 'Commentaires',
	'label_date_reglement' => 'Date de règlement',
	'label_id_facture' => 'Id facture',
	'label_montant' => 'Montant',

	// R
	'retirer_lien_reglement' => 'Retirer ce règlement',
	'retirer_tous_liens_reglements' => 'Retirer tous les règlements',

	// T
	'texte_ajouter_reglement' => 'Saisir un nouveau règlement',
	'texte_changer_statut_reglement' => 'Ce règlement est :',
	'texte_creer_associer_reglement' => 'Créer et associer un règlement',
	'titre_langue_reglement' => 'Langue de ce règlement',
	'titre_logo_reglement' => 'Logo de ce règlement',
	'titre_reglement' => 'Règlement',
	'titre_reglements' => 'Règlements',
	'titre_reglements_rubrique' => 'Règlements de la rubrique',
);
